using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstWall : Wall
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Path2.position = new Vector3(0, -4, Path1.position.z + 1000f);         
        }
    }
}
