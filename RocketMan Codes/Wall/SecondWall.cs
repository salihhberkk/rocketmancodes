using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondWall : Wall
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Path1.position = new Vector3(0, -4, Path2.position.z + 1000f);
        }
    }
}
