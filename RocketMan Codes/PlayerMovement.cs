using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using DG.Tweening;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float sensivity;
    [SerializeField] private GameObject[] renderers;

    [SerializeField] private float maxVelocityY = 30;
    [SerializeField] private float minVelocityY = -5;

    private bool isAlive = true;
    private bool isFly;

    private Animator animator;
    private Rigidbody rb;

    CharacterState characterState;
    enum CharacterState
    {
        WingClose = 0,
        WingOpen = 1,
    }
    private void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();

        animator.enabled = false;
    }
    private void Update()
    {
        if (!PlayerIsAlive)
        {
            foreach (var renderer in renderers)
            {
                renderer.SetActive(false);
            }
        }
        if (isFly)
        {
            
            if (PlayerIsAlive)
            {
                Physics.gravity = new Vector3(0, -7f, 0);
                rb.drag = 0f;
            }
        }
        else
        {
            Physics.gravity = new Vector3(0, -17f, 0);
            if (PlayerIsAlive)
            {
                rb.drag = 0.05f;
            }
        }
    }
    public bool PlayerIsAlive
    {
        get => isAlive;
        set
        {
            isAlive = value;
        }
    }
    public void StartPlayerSwipe()
    {
        LeanTouch.OnFingerUpdate += HandlePlayer;
    }
    public void StopPlayerSwipe()
    {
        LeanTouch.OnFingerUpdate -= HandlePlayer;
    }
    public void StopPlayerControl()
    {
        LeanTouch.OnFingerUpdate -= HandlePlayer;
        SetState(CharacterState.WingClose);
        rb.drag = 3f;
        rb.angularDrag = 4f;
    }
    private void HandlePlayer(LeanFinger obj)
    {
        if (animator.enabled == false)
        {
            animator.enabled = true;
        }
        if (Input.GetMouseButton(0))
        {
            OpenWing();
            isFly = true;
            rb.AddForce(Vector3.forward * 0.5f);
            foreach (var renderer in renderers)
            {
                renderer.SetActive(true);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            CloseWing();
            foreach (var renderer in renderers)
            {
                renderer.SetActive(false);
            }

            rb.AddTorque(transform.right * 1000);
            isFly = false;
        }
        Haptic.Instance.HapticFeedback(MoreMountains.NiceVibrations.HapticTypes.LightImpact);
        var angleRotation = Mathf.Clamp(obj.ScaledDelta.x, -30, 30);            //smooth rotation
        transform.DORotateQuaternion( Quaternion.Euler(50, angleRotation, angleRotation * -1), 0.5f);
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(50, angleRotation, angleRotation * -1), 3f);
        rb.angularVelocity = Vector3.zero;
        

        gameObject.transform.position += Vector3.right * (obj.ScaledDelta.x * sensivity / 100);
        var clampXPos = Mathf.Clamp(gameObject.transform.position.x, -100, 100);
        gameObject.transform.position = new Vector3(clampXPos, gameObject.transform.position.y,
              gameObject.transform.position.z);

    }

    public void OpenWing()
    {
        SetState(CharacterState.WingOpen);
    }
    public void CloseWing()
    {
        SetState(CharacterState.WingClose);
    }
    private void SetState(CharacterState state)
    {
        characterState = state;

        animator.SetInteger("Wing", (int)state);
    }

}

