using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform target;
    private bool PlayerIsFired = false;
    [SerializeField] private Vector3 offset;
    [SerializeField] private Vector3 offset2;

    private PlayerMovement player;

    private void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }
    void LateUpdate()
    {
        if (!PlayerIsFired)
        {
            transform.position = (target.transform.position + offset);
        }
        else
        {
            transform.position = (target.transform.position + offset2);
            if(player.GetComponent<Rigidbody>().velocity.y < -5)
            {
                transform.DORotateQuaternion(Quaternion.Euler(30, 0, 0), 2f);
            }
            else
            {
                transform.DORotateQuaternion(Quaternion.Euler(20, 0, 0), 2f);
            }
        }
    }
    public void SetRotate()
    {
        transform.rotation = Quaternion.identity;
        PlayerIsFired = true;
    }
}
