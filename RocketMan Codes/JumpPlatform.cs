using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPlatform : MonoBehaviour
{
    [SerializeField] private float force;
    [SerializeField] private Transform forcePos;
    [SerializeField] private GameObject explosionFx;

    private PlayerMovement player;
    private void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            var smokeParticle = ObjectPooler.Instance.GetPooledObject("Smoke");
            smokeParticle.transform.position = collision.transform.position;
            smokeParticle.SetActive(true);
            smokeParticle.GetComponent<ParticleSystem>().Play();

            if (!player.PlayerIsAlive)
            {
                return;
            }
            Haptic.Instance.HapticFeedback(MoreMountains.NiceVibrations.HapticTypes.HeavyImpact);
            player.GetComponent<Rigidbody>().velocity = Vector3.zero;
            player.GetComponent<Rigidbody>().AddForce(new Vector3(0, forcePos.transform.rotation.y, forcePos.transform.rotation.z) * force);
            //player.GetComponent<Rigidbody>().AddForce(new Vector3(0,1,1)  * force);

        }
    }
}
