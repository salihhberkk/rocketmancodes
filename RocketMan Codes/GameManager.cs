using UnityEngine;
using Lean.Touch;

public class GameManager : MonoSingleton<GameManager>
{
    private CameraMovement camera;
    private Rigidbody playerRb;

    [SerializeField] private GameObject player;


    [SerializeField] private float fireForce;
    [SerializeField] private float stickSwipeSens;

    internal int hapticOn;
    private float distance;
    private Stick stick;
    private void Awake()
    {
        Application.targetFrameRate = 60;
    }
    private void Start()
    {
        stick = FindObjectOfType<Stick>();
        playerRb = player.GetComponent<Rigidbody>();
        camera = FindObjectOfType<CameraMovement>();
    }
    internal void StartGame()
    {
        LeanTouch.OnFingerUpdate += HandleCannon;
        UIManager.Instance.ShowPanel(PanelType.Game);
    }
    public void FinishGame()
    {
        player.GetComponent<PlayerMovement>().PlayerIsAlive = false;
        Invoke("WaitSecond", 1.5f);
    }
    private void WaitSecond()
    {
        UIManager.Instance.ShowPanel(PanelType.Lose);
    }
    private void HandleCannon(LeanFinger obj)
    {
        
        if (Input.GetMouseButton(0))
        {
            distance = (obj.StartScreenPosition.x - obj.LastScreenPosition.x) / stickSwipeSens;
            if(distance > 1)
            {
                distance = 1;
            }
            if (distance < 0.1f)
            {
                distance = 0.1f;
            }
            stick.SetAnimationProgress(distance);

        }
        if (Input.GetMouseButtonUp(0))
        {
            camera.SetRotate();
            player.transform.SetParent(null);
            playerRb.isKinematic = false;
            playerRb.AddForce(player.transform.forward * (fireForce * distance * 10) );
            playerRb.AddTorque(transform.right * 300);
           
            LeanTouch.OnFingerUpdate -= HandleCannon;
            player.GetComponent<PlayerMovement>().StartPlayerSwipe();
        }
    }
    public float CheckPlayerDistance()
    {
        return player.transform.position.z;
    }
}
