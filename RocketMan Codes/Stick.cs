using UnityEngine;

public class Stick : MonoBehaviour
{
    [SerializeField] private Animator _animator;


    public void SetAnimationProgress(float ratio)
    {
        _animator.SetFloat("speed", ratio);
    }
}
